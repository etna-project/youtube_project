import {createRouter, createWebHistory} from 'vue-router'
import Index from '../views/index.vue'
import Profile from "@/views/profile.vue";
import uploadVideo from "@/views/video/uploadVideo.vue";
import Login from "@/views/Login.vue";
import Signup from "@/views/Signup.vue";

const routes = [
    {
        path: '/',
        name: 'home',
        component: Index
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/signup',
        name: 'signup',
        component: Signup
    },
    {
        path: '/upload',
        name: 'video_upload',
        component: uploadVideo
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile
    },
]
const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
