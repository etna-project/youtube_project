from flask import Flask, request, jsonify, g

import smtplib
from email.mime.text import MIMEText
from subprocess import Popen, PIPE

app = Flask(__name__)

def send_email(email, email_type):
    
    msg = MIMEText("Information mis a jour avec succes")
    msg["Subject"] = "Vos informations"
    msg["From"] = "brunelleagboton@gmail.com"
    msg["To"] = email

    # Enregistrer l'e-mail dans un fichier temporaire
    with open("/tmp/email.txt", "w") as tmp_file:
        tmp_file.write(msg.as_string())

    # Utiliser Postfix pour envoyer l'e-mail
    try:
        p = Popen(["/usr/sbin/sendmail", "-t", "-i"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        p.communicate(input=msg.as_bytes())
        return True
    except Exception as e:
        print(f"Erreur lors de l'envoi de l'e-mail : {e}")
        return False



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)