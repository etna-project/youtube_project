import Axios from "@/_services/caller.service";
import {UserService} from "./user.service";

const listVideo = (params) => {
  return Axios.get("videos", {params})
}

const upload = (name, source) => {
  const user = UserService.decodeToken()
  const formData = new FormData();
  formData.append('name', name);
  formData.append('source', source);
  return Axios.post(`/user/${user.user_id}/video`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    }
  })
}

const encode_video = (params) => {
  return Axios.post(`/encode_video`, {params})
}


export const VideoService = {
  listVideo, upload, encode_video
}
