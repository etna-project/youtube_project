import Axios from "@/_services/caller.service";

const auth = (user) => {
    return Axios.post("auth", user)
}

const register = (user) => {

    return Axios.post("user", user)
}


const editUser = (user) => {
    console.log(user)
    // if (user.password.length > 0) {
    //     const emailData = {
    //         email: user.email,
    //         type: 'changement de mot de passe',
    //     };
    //     Axios.post('send_email', emailData)
    //         .then(response => {
    //             if (response === 'True') {
    //                 console.log("Mail envoye avec succes !")
    //             }
    //         })
    //         .catch(error => {
    //             return error
    //         });
    // }
    const id = UserService.decodeToken().user_id
    return Axios.put(`user/${id}`, user)
}
const logout = () => {
    localStorage.removeItem('token');
}
const getToken = () => {
    return localStorage.getItem('token')
}

export const decodeToken = () => {
    const token = getToken()
    if (token) {
        const [,payloadEncoded] = token.split(".");
        return JSON.parse(atob(payloadEncoded));
    } else {
        return false
    }
}
const saveToken = (token) => {
    localStorage.setItem('token', token)
}

const isLogged = () => {
    const token = localStorage.getItem("token")
    return !!token
}


export const UserService = {
    auth, register, editUser, logout, getToken, decodeToken, isLogged, saveToken
}
