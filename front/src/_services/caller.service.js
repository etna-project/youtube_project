import axios from "axios";
import {UserService} from "./user.service";

const Axios = axios.create({
        baseURL: 'http://0.0.0.0:5000'
    }
)

axios.interceptors.response.use(
    (response) => {
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type, Accept, Access-Control-Allow-Origin ';
        response.headers['Access-Control-Allow-Origin'] = '*';
        response.headers['Content-Type'] = 'application/json';

        if (response) {
            console.log(response);
        }

        return response;
    },
    (error) => {
        if (!error.response) {
            // Gérer le cas où error.response n'est pas défini
            console.error('Erreur de réponse non définie:', error.message);
            // this.$store.commit("SetDisplayNotif", {d: true, mes: error.message});
            return Promise.reject(error);
        }

        if (error.response.status === 401) {
            // Gérer le cas où le statut de réponse est 401
            console.error('Erreur de statut 401:', error.message);
            // UserService.logout();
            // this.$router.push('/');
        } else {
            // Gérer d'autres erreurs de réponse
            console.error('Erreur de réponse:', error.response.message);
            // this.$store.commit("SetDisplayNotif", {d: true, mes: error.response.message});
            return Promise.reject(error);
        }
    }
);

Axios.interceptors.request.use(request => {
        request.headers["Access-Control-Allow-Headers"] = "Content-Type, Accept, Access-Control-Allow-Origin "
        request.headers["Content-Type"] = 'application/json'
        request.headers["Cross-Origin-Request"] = '*'
        request.headers.Accept = '*'
        request.headers["Access-Control-Allow-Origin"] = '*'
        const token = UserService.getToken()
        if (token) {
            request.headers.Authorization = 'Bearer ' + token
        }
        return request
    }
)
export default Axios
