import {createStore} from 'vuex';
// @ts-ignore
import {UserService} from "../_services";
import router from "@/router";

const store = createStore({
  state: {
    notifDisplay: false,
    notifMessage: '',
    user: {
      id: 0,
      login: '',
      role: 0,
    },
    isLoggedIn: false,
    isVisible: false,
    modalMode: 'Login',
    status: '',
    errors: []
  },
  getters: {
    authStatus(state) {
      return state.status
    },
    getNotif(state) {
      return state.notifDisplay
    },
    getNotifMessage(state) {
      return state.notifMessage
    },
    currentUser(state) {
      return state.user;
    },
    isAuthenticated(state) {
      return state.isLoggedIn;
    },
    isVisible(state) {
      return state.isVisible
    },
    getMode(state) {
      return state.modalMode
    },
    getErrors(state) {
      return state.errors
    }
  },
  actions: {
    loadUserFromLocalStorage({commit}) {
      let userData = UserService.decodeToken()
      if (!userData) {
        userData = {
          id: 0,
          login: '',
          role: 0,
        }
        commit('setUser', userData);
      }
      commit('setUser', userData);
    },
    checkAuthentication({commit}) {
      const logged = UserService.isLogged()
      if (logged) {
        commit('setIsLoggedIn', true);
      } else {
        commit('setIsLoggedIn', false);
      }
    },
    async signup({commit}, user) {
      console.log(user)
      const errs = [];
      commit('authRequest');
      if (user.email.length === 0) {
        errs.push('email_required');
      }
      if (user.password.length === 0) {
        errs.push('password_required');
      }
      if (errs.length > 0) {
        commit('authError', errs);
        return;
      }
      await UserService.register(user)
          .then((res) => {
            console.log(res);
            commit('authSuccess');
            commit('hide', null, {root: true});
          })
          .catch((err) => {
            commit('authError', [err.response.data]);
          });
    },
    async login({commit}, credentials) {
      try {
        await UserService.auth(credentials)
            .then((res) => {
              UserService.saveToken(res.data.data);
            })
        let userData = UserService.decodeToken()
        console.log(userData)
        commit('setUser', userData);
        commit('setIsLoggedIn', true);
      } catch (error) {
        console.log(error.message);
        throw error;
      }
    },
    async logout({commit}) {
      try {
        await UserService.logout();
        commit('logout');
        let user = {
          id: 0,
          login: '',
          role: 0,
        }
        commit('setUser', user)
        await router.push('/login')
      } catch (error) {
        // Gérez les erreurs de déconnexion ici
        console.error(error);
        throw error; // Propagez l'erreur pour que le composant puisse la gérer
      }
    },
    show({commit}) {
      return new Promise((resolve) => {
        commit('show')
        resolve()
      })
    },
    hide({commit}) {
      return new Promise((resolve) => {
        commit('hide')
        resolve()
      })
    },
    setAuthenticated({commit}) {
      commit('authSuccess');
    },
    changeMode({commit}, mode) {
      return new Promise((resolve) => {
        commit('changemode', mode)
        resolve()
      })
    }
  },
  mutations: {
    SetDisplayNotif(state, payload) {
      state.notifDisplay = payload.d
      state.notifMessage = payload.mes
    },
    setIsLoggedIn(state, isLogged) {
      state.isLoggedIn = isLogged
    },
    setUser(state, user) {
      state.user = {
        id: user.id,
        login: user.pseudo,
        // role: user.role,
      };
    },
    logout(state) {
      state.isLoggedIn = false;
    },
    show(state) {
      state.isVisible = true
    },
    hide(state) {
      state.isVisible = false
      state.modalMode = 'Login'
    },
    changemode(state, mode) {
      state.modalMode = mode
    },
    reset(state) {
      state.status = '';
      state.errors = [];
    },
    authRequest(state) {
      state.status = 'loading';
      state.isLoggedIn = false;
      state.errors = [];
    },
    authSuccess(state) {
      state.status = 'success';
      state.isLoggedIn = true;
      state.errors = [];
    },
    authError(state, errors) {
      state.status = 'error';
      state.isLoggedIn = false;
      state.errors = errors;
    },
  },

});

export default store;
