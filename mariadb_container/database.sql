DROP SCHEMA IF EXISTS `my_youtube`;
CREATE SCHEMA IF NOT EXISTS `my_youtube` DEFAULT CHARACTER SET utf8;
USE `my_youtube`;

-- Table `my_youtube`.`role` --

CREATE TABLE IF NOT EXISTS `my_youtube`.`role`
(
    `id`         INT AUTO_INCREMENT,
    `label`      VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;


-- Table `my_youtube`.`user` --

CREATE TABLE IF NOT EXISTS `my_youtube`.`user`
(
    `id`         INT AUTO_INCREMENT,
    `username`   VARCHAR(45)  NOT NULL,
    `password`   VARCHAR(100) NOT NULL,
    `created_at` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `username_UNIQUE` (`username` ASC)
) ENGINE = InnoDB;

-- Table `my_youtube`.`video` --

CREATE TABLE IF NOT EXISTS `my_youtube`.`video`
(
    `id`         INT AUTO_INCREMENT,
    `name`       VARCHAR(45)  NOT NULL,
    `duration`   INT          NULL,
    `user_id`    INT          NOT NULL,
    `source`     VARCHAR(150) NOT NULL,
    `created_at` DATETIME     NULL DEFAULT CURRENT_TIMESTAMP,
    `view`       INT          NULL DEFAULT 0,
    `enabled`    TINYINT(1)   NULL DEFAULT 1,
    PRIMARY KEY (`id`),
    INDEX `fk_video_user_idx` (`user_id` ASC),
    CONSTRAINT `fk_video_user` FOREIGN KEY (`user_id`) REFERENCES `my_youtube`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB;

--Table `my_youtube`.`video_format` --

CREATE TABLE IF NOT EXISTS `my_youtube`.`video_format`
(
    `id`       INT AUTO_INCREMENT,
    `code`     longtext    NOT NULL,
    `uri`      VARCHAR(45) NOT NULL,
    `video_id` INT         NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_video_format_video1_idx` (`video_id` ASC),
    CONSTRAINT `fk_video_format_video1` FOREIGN KEY (`video_id`) REFERENCES `my_youtube`.`video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB;


-- Table `my_youtube`.`comment` --

CREATE TABLE IF NOT EXISTS `my_youtube`.`comment`
(
    `id`       INT AUTO_INCREMENT,
    `body`     LONGTEXT NULL,
    `user_id`  INT      NOT NULL,
    `video_id` INT      NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_comment_user1_idx` (`user_id` ASC),
    INDEX `fk_comment_video1_idx` (`video_id` ASC),
    CONSTRAINT `fk_comment_user1` FOREIGN KEY (`user_id`) REFERENCES `my_youtube`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_comment_video1` FOREIGN KEY (`video_id`) REFERENCES `my_youtube`.`video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB;