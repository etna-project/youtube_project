from flask_bcrypt import Bcrypt
from flask_jwt_extended import create_access_token, get_jwt_identity

from ..model.user_model import User

bcrypt = Bcrypt()


def authenticate_user(data):
    username = data['username']
    password = data['password']

    user = User.query.filter_by(username=username).first()

    if user and bcrypt.check_password_hash(user.password, password):
        return user

    return None


def create_access_token_for_user(user):
    access_token = create_access_token(identity=user.id)
    return access_token


def get_current_user():
    current_user_id = get_jwt_identity()
    return User.query.get(current_user_id)
