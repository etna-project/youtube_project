import os
import subprocess

from flask import current_app
from flask_jwt_extended import get_jwt_identity
from werkzeug.utils import secure_filename

from api import db
from api.model.user_model import User
from api.model.video_model import Video


def add_video_to_user(user_id, data, file):
    current_user = get_jwt_identity()
    name = data.get("name")
    if not name or not file:
        return None, {"error": "Name and source file are required."}, 400

    if user_id != current_user:
        return None, {"error": "You are not authorized to perform this action."}, 403

    existing_user = User.query.filter_by(id=user_id).first()

    if not existing_user:
        return None, {"error": "User not found."}, 404

    # Gestion du fichier
    filename = secure_filename(file.filename)
    file_path = f"{current_app.config['UPLOAD_FOLDER']}/{filename}"
    file.save(file_path)

    new_video = Video(name=name, source=file_path, user_id=user_id)
    db.session.add(new_video)
    db.session.commit()

    # Inclure l'identité de l'utilisateur dans la réponse
    return new_video, {
        "message": "Video added successfully",
        "video": {
            "id": new_video.id,
            "name": new_video.name,
            "duration": new_video.duration,
            "source": new_video.source,
        },
        "logged_in_as": current_user
    }, 201


def get_video_list_by_user(user_id, data):
    video_query = Video.query

    per_page = int(data.get("perPage", 5))
    page = int(data.get("page", 1))

    current_user_id = get_jwt_identity()
    if user_id != current_user_id:
        return None, {"error": "You are not authorized to perform this action."}, 403

    videos_paginations = video_query.filter_by(user_id=user_id).paginate(page=page, per_page=per_page, error_out=False)
    total_pages = videos_paginations.pages

    if page <= 0 or page > total_pages:
        return None, {"error": "Invalid page number"}, 418

    videos = videos_paginations.items
    print(videos)
    video_list = []
    for video in videos:
        video_dict = {
            "id": video.id,
            "name": video.name,
            "user": video.user_id,
            "duration": video.duration,
            "source": video.source,
            "view": video.view,
            "created_at": video.created_at,
        }
        video_list.append(video_dict)

    return video_list, {"current": page, "total": total_pages}, 200


def get_all_videos(data):
    video_query = Video.query

    per_page = int(data.get("perPage", 5))
    page = int(data.get("page", 1))

    videos_paginations = video_query.paginate(page=page, per_page=per_page, error_out=False)
    total_pages = videos_paginations.pages
    if not video_query.all():
        return None, {"error": "No videos found"}, 404
    if page <= 0 or page > total_pages:
        print(page, total_pages)
        return None, {"error": "Invalid page number"}, 400

    videos = videos_paginations.items

    video_list = []
    for video in videos:
        video_dict = {
            "id": video.id,
            "name": video.name,
            "duration": video.duration,
            "source": video.source,
            "view": video.view,
            "created_at": video.created_at,
        }
        video_list.append(video_dict)
    return video_list, {"current": page, "total": total_pages}, 200


def encode_video(video_id, source):
    try:
        resolutions = [1080, 720, 480, 360, 240]

        if not source:
            return {"message": "Bad Request", "code": 400, "data": []}, 400

        resolution = 1080

        ffmpeg_path = "/usr/bin/ffmpeg"

        video = Video.query.get(video_id)
        if not video:
            return {"error": "Video not found", "code": 404, "data": []}, 404

        # Sauvegarder le fichier téléchargé temporairement
        filename = secure_filename(source.filename)
        temp_path = os.path.join(current_app.config['UPLOAD_FOLDER'], filename)
        source.save(temp_path)

        for target_resolution in resolutions:
            if target_resolution <= resolution:
                output_video = f"{video.name}_encoded_{target_resolution}.mp4"
                output_path = os.path.join(current_app.config['UPLOAD_FOLDER'], output_video)
                print(output_path)
                command = [
                    ffmpeg_path,
                    "-y",
                    "-i", temp_path,
                    "-vf", f"scale={target_resolution}:trunc(ow/a/2)*2",
                    "-c:v", "libx264",
                    "-c:a", "copy",
                    output_path
                ]
                subprocess.run(command)

        # Supprimer le fichier temporaire après utilisation
        os.remove(temp_path)

        video_data = {
            "id": video.id,
            "name": video.name,
            "duration": video.duration,
            # Ajoutez d'autres propriétés selon vos besoins
        }

        return {"message": "Video encoding successful", "data": video_data}, 200

    except subprocess.CalledProcessError as e:
        return {"error": f"Encoding error: {str(e)}"}, 500
    except Exception as e:
        return {"error": f"Unexpected error: {str(e)}"}, 500


# @TODO: Add update_video function


def delete_video(video_id):
    current_user_id = get_jwt_identity()
    video = Video.query.get(video_id)
    print(video)
    if video.user_id != current_user_id:
        return None, {"error": "You are not authorized to perform this action."}, 403

    if video:
        db.session.delete(video)
        db.session.commit()
        return video, {"message": "Video deleted successfully."}, 204
    else:
        return None, {"error": "video not found."}, 404
