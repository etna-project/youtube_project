from flask_bcrypt import Bcrypt
from flask_jwt_extended import get_jwt_identity

from api import db
from api.model.user_model import User

bcrypt = Bcrypt()


def create_user(data):
    username = data.get("username")
    password = data.get("password")
    print(data)
    if not username or not password:
        return None, {"error": "Username and password are required."}, 400

    existing_user = User.query.filter_by(username=username).first()

    if existing_user:
        return None, {"error": "Username already in use."}, 400
    password = bcrypt.generate_password_hash(password).decode("utf-8")
    new_user = User(username=username, password=password)
    db.session.add(new_user)
    db.session.commit()

    return new_user, {
        "message": "User registered successfully",
        "user":
            {
                "id": new_user.id,
                "username": new_user.username
            }
    }, 201


def get_user(user_id):
    current_user_id = get_jwt_identity()

    if user_id != current_user_id:
        return None, {"error": "You are not authorized to perform this action."}, 403

    user = User.query.get(user_id)

    if user:
        user_info = {
            "id": user.id,
            "username": user.username,
            "created_at": user.created_at,
        }
        return user_info, 200
    else:
        return None, {"error": "User not found."}, 404


def get_users_list(per_page, page, username=None):
    query = User.query

    if username:
        query = query.filter(User.username == username)

    users_pagination = query.paginate(page=page, per_page=per_page, error_out=False)
    total_pages = users_pagination.pages

    if page <= 0 or page > total_pages:
        return None, {"error": "Invalid page number"}, 400

    users = users_pagination.items

    user_list = []
    for user in users:
        user_dict = {
            "id": user.id,
            "username": user.username,
            "created_at": user.created_at,
        }
        user_list.append(user_dict)

    return user_list, {"current": page, "total": total_pages}, 200


def update_user(user_id, data):
    current_user_id = get_jwt_identity()

    if user_id != current_user_id:
        return None, {"error": "You are not authorized to perform this action."}, 403

    username = data.get("username")

    if not username:
        return None, {"error": "Username is required."}, 400

    user = User.query.get(user_id)

    if user:
        user.username = username
        db.session.commit()
        return user, {
            "message": "User information updated successfully.",
            "id": user.id,
            "username": user.username,
            "created_at": user.created_at
        }, 200
    else:
        return None, {"error": "User not found."}, 404


def delete_user(user_id):
    current_user_id = get_jwt_identity()

    if user_id != current_user_id:
        return None, {"error": "You are not authorized to perform this action."}, 403

    user = User.query.get(user_id)

    if user:
        db.session.delete(user)
        db.session.commit()
        return user, {"message": "User deleted successfully."}, 204
    else:
        return None, {"error": "User not found."}, 404
