from marshmallow import Schema, fields


class CommentSchema(Schema):
    id = fields.Int(dump_only=True)
    body = fields.Str(required=True)
    password = fields.Str(required=True)
    created_at = fields.DateTime(dump_only=True)
