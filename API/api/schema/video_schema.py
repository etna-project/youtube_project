from marshmallow import Schema, fields


class VideoSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    duration = fields.Int()
    user_id = fields.Int()
    source = fields.Str()
    created_at = fields.DateTime()
    view = fields.Int()
    enabled = fields.Bool()
