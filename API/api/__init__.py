from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from api.config import Config

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    register(app)
    CORS(app)
    app.config.from_object(Config)
    db.init_app(app)
    Migrate(app, db)
    Bcrypt(app)
    return app


def register(app):
    from api.route.user_route import user_bp
    from api.route.video_route import video_bp
    app.register_blueprint(user_bp)
    app.register_blueprint(video_bp)
