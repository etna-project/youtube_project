from flask import Blueprint, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

from ..service.video_service import delete_video, get_video_list_by_user, get_all_videos, add_video_to_user, \
    encode_video

video_bp = Blueprint('video_bp', __name__)


@video_bp.route('/user/<int:user_id>/video', methods=['POST'])
@jwt_required()
def add_video(user_id):
    current_user = get_jwt_identity()
    data = request.form
    file = request.files.get("source")

    video, response, status_code = add_video_to_user(user_id, data, file)

    if video:
        # Inclure l'identité de l'utilisateur dans la réponse
        return jsonify(response, current_user), status_code
    else:
        return jsonify(response), status_code


@video_bp.route('/user/<int:user_id>/video', methods=['GET'])
@jwt_required()
def videos_list_by_user(user_id):
    data = request.get_json()
    video_list, pager_info, status_code = get_video_list_by_user(user_id, data)
    if video_list is not None:
        return jsonify(
            {
                "message": "OK",
                "data": video_list,
                "pager": pager_info
            }), status_code
    else:
        return jsonify(
            {
                "error": pager_info["error"]
            }), status_code


@video_bp.route('/videos', methods=['GET'])
def videos_list():
    data = request.get_json()

    video_list, pager_info, status_code = get_all_videos(data)

    if video_list is not None:
        return jsonify(
            {
                "message": "OK",
                "data": video_list,
                "pager": pager_info
            }), status_code
    else:
        return jsonify(
            {
                "error": pager_info["error"]
            }), status_code


@video_bp.route('/video/<int:video_id>', methods=['PATCH'])
def encode_video_route(video_id):
    source = request.files.get("source")
    result, status_code = encode_video(video_id, source)
    return jsonify(result), status_code


@video_bp.route('/video/<int:user_id>', methods=['DELETE'])
@jwt_required()
def video_delete(video_id):
    try:
        deleted_video_info, status_code = delete_video(video_id)

        if deleted_video_info:
            return jsonify(deleted_video_info), status_code
        else:
            return jsonify({"error": f"An error occurred or user not found."}), status_code

    except Exception as e:
        return jsonify({"error": f"An error occurred: {e}"}), 500
