from flask import Blueprint, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity

from ..service.auth_service import authenticate_user, create_access_token_for_user, get_current_user
from ..service.user_service import create_user, get_user, update_user, delete_user, get_users_list

user_bp = Blueprint('user_bp', __name__)


@user_bp.route('/register', methods=['POST'])
def register_user():
    data = request.get_json()

    user, response, status_code = create_user(data)

    if user:
        return jsonify(response), status_code
    else:
        return jsonify(response), status_code


@user_bp.route('/login', methods=['POST'])
def login_user():
    data = request.get_json()
    user = authenticate_user(data)

    if user:
        access_token = create_access_token_for_user(user)
        return jsonify({'message': 'Login successful', 'data': access_token}), 200
    else:
        return jsonify({'message': 'Invalid credentials'}), 401


@user_bp.route('/profile', methods=['GET'])
@jwt_required()
def get_user_profile():
    current_user = get_current_user()
    return jsonify(
        {
            'user':
                {
                    'id': current_user.id,
                    'username': current_user.username
                }
        })


@user_bp.route('/users', methods=['GET'])
def users_list():
    try:
        data = request.get_json()

        per_page = int(data.get("perPage", 5))
        page = int(data.get("page", 1))

        username = data.get("username", None)

        user_list, pager_info, status_code = get_users_list(per_page, page, username)

        if user_list is not None:
            return jsonify(
                {
                    "message": "OK",
                    "data": user_list,
                    "pager": pager_info
                }
            ), status_code
        else:
            return jsonify({"error": pager_info["error"]}), status_code

    except Exception as e:
        return jsonify(
            {
                "error": f"An error occurred: {e}"
            }), 500


@user_bp.route('/user/<int:user_id>', methods=['GET'])
@jwt_required()
def user_get(user_id):
    try:
        user_info, status_code = get_user(user_id)

        if user_info:
            return jsonify({"user": user_info}), status_code
        else:
            return jsonify({"error": f"An error occurred or user not found."}), status_code

    except Exception as e:
        return jsonify({"error": f"An error occurred: {e}"}), 500


@user_bp.route('/user/<int:user_id>', methods=['PUT'])
@jwt_required()
def user_update(user_id):
    try:
        data = request.get_json()

        user, updated_user_info, status_code = update_user(user_id, data)

        if updated_user_info:
            return jsonify(
                {
                    "user": updated_user_info,
                }), status_code
        else:
            return jsonify({"error": f"An error occurred or user not found."}), status_code

    except Exception as e:
        return jsonify({"error": f"An error occurred: {e}"}), 500


@user_bp.route('/user/<int:user_id>', methods=['DELETE'])
@jwt_required()
def user_delete(user_id):
    try:
        user, deleted_user_info, status_code = delete_user(user_id)

        if deleted_user_info:
            return jsonify(deleted_user_info), status_code
        else:
            return jsonify({"error": f"An error occurred or user not found."}), status_code

    except Exception as e:
        return jsonify({"error": f"An error occurred: {e}"}), 500
