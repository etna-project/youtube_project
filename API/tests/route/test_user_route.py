import pytest
from flask import Flask
from api.route.user_route import user_bp


@pytest.fixture
def app():
    app = Flask(__name__)
    app.register_blueprint(user_bp)
    return app


def test_user_list_route(data):
    assert data



def test_user_get_route():
    # Réalisez des tests pour la route /user/<int:user_id>
    # Assurez-vous de tester différents scénarios, par exemple, avec un utilisateur existant et inexistant
    pass

# Ajoutez d'autres tests pour les routes, selon vos besoins
