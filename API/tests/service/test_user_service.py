from unittest.mock import patch

from api.service.user_service import get_users_list


# def test_create_user(new_user):
#     assert new_user.username == 'patkennedy79@gmail.com'
#     assert new_user.password == 'FlaskIsAwesome'
#

@patch('api.service.user_service.get_users_list')
def test_get_users_list(mock_get_users_list):
    """
    GIVEN: The function get_users_list
    WHEN: The function is called with the parameters
    THEN: The function should return the users list
    :param mock_get_users_list:
    :return:
    """
    per_page = 5
    page = 1
    username = None

    user_list, pager_info, status_code = get_users_list(per_page, page, username)

    assert user_list is not None
    assert pager_info["current"] == page
    assert pager_info["total"] > 0
    assert status_code == 200


def test_get_users_list_with_username(self):
    # Test case 3: Test with username filter
    per_page = 5
    page = 1
    username = "john.doe"

    user_list, pager_info, status_code = get_users_list(per_page, page, username)

    assert user_list is not None
    assert pager_info["current"] == page
    assert pager_info["total"] > 0
    assert status_code == 200


def test_get_users_list_invalid_page_number(self):
    # Test case 2: Test with invalid page number
    per_page = 5
    page = -1
    username = None

    user_list, error, status_code = get_users_list(per_page, page, username)

    assert user_list is None
    assert error["error"] == "Invalid page number"
    assert status_code == 400


@patch('api.service.user_service.get_jwt_identity')
def test_update_user(mock_get_jwt_identity):
    # Réalisez des tests pour la fonction update_user
    pass


@patch('api.service.user_service.get_jwt_identity')
def test_delete_user(mock_get_jwt_identity):
    # Réalisez des tests pour la fonction delete_user
    pass

# Ajoutez d'autres tests pour le service, selon vos besoins
